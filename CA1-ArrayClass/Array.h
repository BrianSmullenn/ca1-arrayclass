#ifndef ARRAY_H
#define ARRAY_H
#include <iostream>
template <class Datatype>
class Array
{
private:
	//attributes are a pointer to array and size of array.
	Datatype* m_array; //member vars no be public
	int m_size;
	float m_grow_size;
	int m_no_of_elements;

	//Binary Search. Much faster. than linear
	//Complexity = log2(n)
	//takes in a value to find and returns the index. 
	//-1 means not found
	//got help from : http://mycodinglab.com/binary-search-algorithm-c/
	//made private so it cant be accessed from main. 
	//Although interface wont allow that anyway.
	//Timing: with 500000 elements look for 75 took 0ms
	//search for missing element took 2ms
	int bsearch(int first,int last, Datatype p_searchFor)
	{
		 int index;
 
		 if (first > last)
		 {
			index = -1;
		 }
 
		 else
		 {
			int mid = (first + last)/2;
 
			if (p_searchFor == m_array[mid])
			{
			index = mid;
			}
			else
			{
				//check and recursiveness
				if (p_searchFor < m_array[mid])
				{
				index = bsearch(first, mid-1, p_searchFor);
				}
				else
				{
				index = bsearch(mid+1, last, p_searchFor);
				}
			}
	 } // end of if 
	 return index;
}

public:
	//Should we use initialiser list here? Time to see the answer
	Array(int p_size, float p_growSize)
	{
		//dont allow a shrink here
		if(p_growSize <= 1)
		{
			p_growSize = 1.5;
			cout << "Set to default of 1.5 you snake." << endl;
		}
		//allocate enough memory for array
		m_array = new Datatype[p_size];//new makes me nervous, remember to tody up later. //Delete in destructor.
		m_size = p_size;
		m_grow_size = p_growSize;
		m_no_of_elements = 0;	
	}
	
	//pretty self explanatory
	//just sets the no_of elements to 0 again
	//size stays the same
	//complextity of 0(1)
	void clear()
	{
		m_no_of_elements = 0;
	}

	//destructor method. Needed for dynamically allocated memory.
	~Array()
	{
		if(m_array != 0)
		{
			delete[] m_array;//square bracket means whole array. VIMPORTANT
		}
		m_array = 0;
	}

	//Param to resise tells the size of the new array
	//Complexity, 0(n) for the copy but requires 2 arrays for a short time. 
	void grow()
	{
		//step 1: create new array
		int newSize = (int)(m_size * m_grow_size);

		Datatype* newarray = new Datatype[newSize];
		//if new array wasnt allocated, then just return 
		if(newarray == 0)
		{
			std::cout << "call to new failed and returned" << std::endl;
			return;
		}

		//loop through and copy all elements.
		for(int index = 0; index < m_no_of_elements; index++)
		{
			newarray[index] = m_array[index];
		}

	//		delete the old array
		if(m_array != 0)
		{
			//make sure to use delete[]
			delete[] m_array;
		}

		//copy the pointer over.
		m_array = newarray;
		newarray = 0;
		m_size = newSize;
	}

	//override the [] operator so it works as you would expect
	Datatype& operator[](int p_index)
	{
		return m_array[p_index];
	}

	//conversion operator to convert and array into pointer so that we can pass array into functions
	operator Datatype*()
	{
		return m_array;
	}

	//Add item to array
	//Sort during add.
	//Complexity 0(n) unless it has to grow. In which case im not sure if its 0(n)^2 or 0(2n)// not really sure
	//got help from shane here. I had some working things as you will see in commits on BB
	//He showed me his code which was undoubtably better. I liked his way and decided to implement similar
    //will* be last in the container will always be O(1), unless
    //growth is encountered, in which case: O(n) again.
	void push(Datatype p_item)
	{
		// if reached full capacity grow to accomadate more elements
		if (this->m_no_of_elements == this->m_size)
		{
			this->grow();
		}

		// Empty container and beyond upper bound
		if (this->m_no_of_elements == 0 || p_item >= this->m_array[this->m_no_of_elements - 1])
		{
			this->m_array[this->m_no_of_elements++] = p_item;
		}

		// Sorted insert
		else
		{
			for (int i = this->m_no_of_elements - 1; i >= 0; --i)
			{
				// Shift right
				this->m_array[i + 1] = this->m_array[i];

            
				if (i == 0)
				{
					this->m_array[0] = p_item;
					break;
				}
				else if (p_item >= this->m_array[i - 1])
				{
					this->m_array[i] = p_item;
					break;
				}
			}

			++this->m_no_of_elements;
		}
	}
	
	//pop removes last element from array
	//complexity of 0(1)
	void pop()
	{
		m_no_of_elements--;
	}
	//this remove function keeps the order of the array.
	//if order is not important there are faster options.
	//remove that cares about order has complexity O(n).
	void remove(int p_index)
	{
		for (int i = p_index; i < m_no_of_elements - 1; i++)
		{
			m_array[i] = m_array[i + 1];
		}
		m_no_of_elements--;
	}

	//getter to get size of array
	int size()
	{
		return m_size;
	}

	//writes the current array out to a file.
	//filename is the paramater and file is created in method.
	//complexity 0(n) 
	//time 500000 elements took 69ms
	bool writeFile(const char* p_fileName)
	{
		//create file
		FILE* outFile = 0;
		int written = 0;

		fopen_s(&outFile, p_fileName, "wb");

		//return if unseccessful
		if (outFile == 0)
		{
			return false;
		}

		//write each element
		//excluding garbage
		for (int i = 0; i < m_no_of_elements; i++)
		{	
			written += fwrite(&m_array[i], sizeof(Datatype), 1, outFile);
		}

		//close the file
		fclose(outFile);

		//return false if there was an error.
		if (written != m_no_of_elements)
		{
			return false;
		}

		//end
		return true;
	}

	//readFile function should read an array in from file.
	//Help from michael(so shane)
	//Time: 500000 ordered ellements = 97ms 0(n)
	//1000 unordered elemenrs = 486ms ??
	bool readFile(const char* p_fileName)
	{
		FILE* inFile = 0;
		int read = 0;

		// open file 
		fopen_s(&inFile, p_fileName, "rb");
		// if the file couldnt be open return false
		if (inFile == 0)
		{
			return false;
		}

		Datatype temp;

		// Iterate over elements
		while (1 == fread(&temp, sizeof(Datatype), 1, inFile))
		{
			push(temp);
		}

		return true;
	}

	//Search for a particular element. Return the index it's at.
	//return of -1 means it wasnt found
	//complexit is 0(n)
	//time: 500000 elements search for 75 took 1ms
	//search for something missing took 2ms
	int linearSearch(Datatype p_searchFor)
	{
		//linear search.
		//More complex but good starting point.
		for(int i = 0; i <= m_no_of_elements; i++)
		{
			if(m_array[i] == p_searchFor)
			{
				return i;
			}
		}
		cout << "Not found!" << endl;
		return -1;
	}
	
	//this just wraps the bsearch method.
	int binarySearch(Datatype p_searchFor)
	{
		return bsearch(0,m_no_of_elements,p_searchFor);
	}
	
	//Nicely print out the important feeatures of the array and each element
	//0(n)
	void print()
	{
		//cout << endl;
		cout << "Size: " << m_no_of_elements << endl;
		cout << "Capacity: " << m_size << endl;
		cout << "Elements: " << endl;
		cout << "-----------------------------" << endl;
		for(int i =0; i < m_no_of_elements; i++)
		{
			cout << i << ": " << m_array[i] << endl;
		}
		cout << "-----------------------------" << endl;
		cout << endl;
	}
};
#endif