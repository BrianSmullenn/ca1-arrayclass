//Main app, demonstration and interface for array class. 

#include <iostream>
#include "Array.h"
#include "Timer.h"

using std::cin;
using std::endl;
using std::cout;

void printMenu();
//void handleInput(int p_input, Array<int> myArray, bool& go);
int getInput();
void randomPopulation(Array<int>& p_array,int p_amount);

// MSVC memory-leak-checking code
#ifdef _MSC_VER
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

int main()
{
	// MSVC memory-leak-checking code
    #ifdef _MSC_VER
		#if defined(DEBUG) | defined(_DEBUG);
            _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
        #endif
    #endif

	//create a timer to use throughout
	Timer t;
	lint after = 0;

	//Used throughout where needed. Here to stop defining in loop.
	int temp = 0;

	//create the array to start before main loop.
	cout << "=========Creating an Array==========" << endl;
	cout << "Enter the initial size of the array" << endl;
	int size = 0;
	cin >> size;
	cout << "Enter the grow size of the array(eg 1.5)" << endl;
	float growSize = 0;
	cin >> growSize;;
	Array<int> myArray(size, growSize);

	//conditional variable, keep going?
	bool go = true;
	//input to control the menu
	int input = 0;
	while(go!=false)
	{
		printMenu();
		input = getInput();
		if(input == 1)
		{
			t.Reset();
			myArray.print();
			after = t.GetMS();
			cout << after << " ms" << endl;
		}
		else if(input == 2)
		{
			cout << "What element would you like to add?" << endl;
			input = getInput();
			cout << endl;
			t.Reset();
			myArray.push(input);
			after = t.GetMS();
			cout << after << " ms" << endl;
		}
		else if(input == 3)
		{
			t.Reset();
			myArray.pop();
			after = t.GetMS();
			cout << after << " ms" << endl;
		}
		else if (input == 4)
		{
			
			cout << "Remove at what index?" << endl;
			cin >> temp;
			t.Reset();
			myArray.remove(temp);
			after = t.GetMS();
			cout << after << " ms" << endl;
		}
		else if(input == 5)
		{
			cout << "What do you want to search for?" << endl;
			input = getInput();
			t.Reset();
			temp = myArray.linearSearch(input);
			after = t.GetMS();
			if(temp!= -1)
			{
			cout << after << " ms" << endl;
			}
			else
			{
				cout << "Not found but took " << after << " ms" << endl; 
			}
		}
		else if(input == 6)
		{
			cout << "What do you want to search for?" << endl;
			input = getInput();
			t.Reset();
			temp = myArray.binarySearch(input);
			after = t.GetMS();
			if(temp!= -1)
			{
				cout << after << " ms" << endl;
			}
			else
			{
				cout << "Not found but took " << after << " ms" << endl; 
			}
		}
		else if(input == 7)
		{
			cout << "Reading in from file..." << endl;
			
			bool isread = false;
			t.Reset();
			isread = myArray.readFile("readUnordered.txt");//change here if needed. Doesnt match up below.
			//isread = myArray.readFile("array.txt");
			after = t.GetMS();
			if(isread)
			{
				cout << "Read successful taking " << after << " ms" << endl;
			}
			else
			{
				cout << "Read unsuccessful taking " << after << " ms" << endl;
			}

		}
		else if(input == 8)
		{
			cout << "Writing array to file..." << endl;
			bool iswritten = false;
			t.Reset();
			iswritten = myArray.writeFile("array.txt");
			after = t.GetMS();
			if(iswritten)
			{
				cout << "Write successful taking " << after << " ms" << endl;
			}
			else
			{
				cout << "Write unsuccessful taking " << after << " ms" << endl;
			}
		}
		else if(input == 9)
		{
			t.Reset();
			myArray.clear();
			after = t.GetMS();
			cout << "cleared array, taking " << after << " ms" << endl;
		}
		else if(input == 10)
		{
			cout << "How many elements would you like to add?" << endl;
			int amount = 0;
			cin >> amount;
			t.Reset();
			randomPopulation(myArray, amount);
			after = t.GetMS();
			cout << "Added " << amount << " random elements, taking " << after << " ms." << endl;
		}
		else if(input == 11)
		{
			go = false;
		}
			//handleInput(input, myArray, go);
	}
	cin.ignore();
	return 0;
}

void printMenu()
{
	cout << "==================================" << endl;
	cout << "1) Print" << endl;
	cout << "2) Push" << endl;
	cout << "3) Pop" << endl;
	cout << "4) Remove" << endl;
	cout << "5) Linear Search" << endl;
	cout << "6) Binary Search" << endl;
	cout << "7) Read from file" << endl;
	cout << "8) Write to file" << endl;
	cout << "9) Clear Array" << endl;
	cout << "10) Random Population" << endl;
	cout << "11) Exit" << endl;
	cout << "==================================" << endl;
}

int getInput()
{
	int input;
	cin >> input;
	return input;
}

//just for testing with ints. I understand its not very reusable or efficient but 
//<Datatype> doesnt seem to work in main
void randomPopulation(Array<int>& p_array,int p_amount)
{
	for(int i = 0; i != p_amount; i++)
	{
		p_array.push(rand()%100 + 1);
	}
	return;
}
